//handles bookmarking for popular social sites
function bookmark(site){
	var info;	
	info = getPageInfo();

	switch(site){

		case 'digg':
			window.open('http://digg.com/remote-submit?phase=2&url='+encodeURIComponent(info["url"])+'&title='+info["title"]+'&bodytext=' + info["description"],'site', 'toolbar=0,status=0,height=450,width=650,scrollbars=yes,resizable=yes'); return false;		
		break;
		case 'facebook':
		    window.open('http://www.facebook.com/sharer.php?u='+encodeURIComponent(info["url"])+'&t='+info["title"]);return false;
		break;    
		case 'myspace':	    				  window.open('http://www.myspace.com/Modules/PostTo/Pages/?u='+encodeURIComponent(info["url"])+'&t='+encodeURIComponent(info["title"])+'&c='+info["description"]);return false;
		break;   
		case 'twitter':
		    if (info["title"].length >= 110){
    	        info["title"] = info["title"].substring(0,109) + '...';
    	    }
		    window.open('http://twitter.com/home/?status='+encodeURIComponent(info["title"])+' '+encodeURIComponent(info["url"])); return false;
		break;
		case 'googlebookmarks':
		    window.open('http://www.google.com/bookmarks/mark?op=edit&output=popup&bkmk='+encodeURIComponent(info["url"])+'&title='+info["title"]); return false;
		break;						
	}
}
//gets the current page meta information
function getPageInfo(){
		var url = document.location.href;
		var title = document.title;
		var author = '';
		var description = '';
		var keywords = '';
		var pageInfo = [];
		if (document.getElementsByName) {
    	  var metaArray = document.getElementsByName('author');
    	  for (var i=0; i<metaArray.length; i++) {
    	    author = metaArray[i].content;
    	  }
    	  var metaArray = document.getElementsByName('description');
    	  for (var i=0; i<metaArray.length; i++) {
    	    description = metaArray[i].content;
    	  }
    	  var metaArray = document.getElementsByName('keywords');
    	  for (var i=0; i<metaArray.length; i++) {
    	    keywords = metaArray[i].content;
    	  }	
    	}
		pageInfo = {"url":url, "title":title, "author":author,"description":description,"keywords":keywords}; 
		return pageInfo;
}
//print links with url info
addThis = '\n' + '<div id="addThis" role="complementary">';
	addThis += '<ul >' + '\n';
	addThis += '<li><a id="facebook" href="javascript:;" onclick="bookmark(\'facebook\');">Facebook</a></li>' + '\n';
		addThis += '<li><a id="twitter" href="javascript:;" onclick="bookmark(\'twitter\');">Twitter</a></li>' + '\n';
		addThis += '<li><a id="myspace" href="javascript:;" onclick="bookmark(\'myspace\');">MySpace</a></li>' + '\n';
		addThis += '<li><a id="digg" href="javascript:;" onclick="bookmark(\'digg\');">Digg</a></li>' + '\n';					
		addThis += '<li> <a id="google" href="javascript:;" onclick="bookmark(\'googlebookmarks\');">Google Bookmarks</a></li>' + '\n';
	addThis += '</ul>' + '\n';
addThis += '</div>' + '\n';

	jQuery(document).ready(function() {
	    $("#share").html(addThis);
	});
